## [jQuery CDN Libraries](http://code.jque.re/)

[![...](http://f.cl.ly/items/1X0I3Z2V0J401k2h0N0N/Image%202014-02-11%20at%204.01.57%20PM.png "...")](http://code.jque.re/)

CDN Libraries: [→ code.jque.re](http://code.jque.re/)

__Why another jQuery CDN site?__
Because the more the merrier. This started as a hobby project and then branched off into something bigger.
I am not the first to have my own jQuery CDN setup. Have a look at these:

- [jsdelivr.com](http://www.jsdelivr.com/)
- [cdnjs.com](http://cdnjs.com/)
- [osscdn.com](http://osscdn.com/)
- [asp.net](http://www.asp.net/ajaxlibrary/cdn.ashx)

[![...](http://f.cl.ly/items/42161a2n23111o2F2O0t/tip.png "Fund me on Gittip")](https://www.gittip.com/SoHiggo//)

###[Donate - gittip.com/SoHiggo](https://www.gittip.com/SoHiggo/)

- [higg.im](http://www.higg.im/)
- [higg.tel](http://higg.tel/)
- [github@higg.im](mailto:github@higg.im)

![...](http://f.cl.ly/items/0E121z1x212X16091Y1k/s.png "...")

       __________  _   __
      / ____/ __ \/ | / /
     / /   / / / /  |/ / 
    / /___/ /_/ / /|  /  
    \____/_____/_/ |_/   
     

- http://code.jque.re/js/jquery-2.0.0.min.js
- http://code.jque.re/js/jquery-2.0.0.js
- http://code.jque.re/js/jquery-1.10.0.min.js
- http://code.jque.re/js/jquery-1.10.0.js
- http://code.jque.re/js/jquery.mobile-1.3.0.js
- http://code.jque.re/js/jquery.mobile-1.3.0.min
- http://code.jque.re/js/jquery.mobile-1.3.0.min.js
- http://code.jque.re/js/ui/jquery-ui.min.js
- http://code.jque.re/js/ui/jquery-ui.js
- http://code.jque.re/js/ui/1.10.3/themes/black-tie/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/blitzer/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/cupertino/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/dark-hive/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/dot-luv/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/eggplant/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/excite-bike/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/flick/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/hot-sneaks/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/humanity/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/le-frog/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/mint-choc/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/overcast/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/pepper-grinder/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/redmond/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/smoothness/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/south-street/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/start/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/sunny/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/swanky-purse/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/trontastic/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/ui-darkness/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/ui-lightness/jquery-ui.min.css
- http://code.jque.re/js/ui/1.10.3/themes/vader/jquery-ui.min.css
- http://code.jque.re/js/all/jquery-1.9.1.min.js
- http://code.jque.re/js/all/jquery-1.9.1.js
- http://code.jque.re/js/all/jquery-1.9.0.min.js
- http://code.jque.re/js/all/jquery-1.9.0.js
- http://code.jque.re/js/all/jquery-1.8.3.min.js
- http://code.jque.re/js/all/jquery-1.8.3.js
- http://code.jque.re/js/all/jquery-1.8.2.min.js
- http://code.jque.re/js/all/jquery-1.8.2.js
- http://code.jque.re/js/all/jquery-1.8.1.min.js
- http://code.jque.re/js/all/jquery-1.8.1.js
- http://code.jque.re/js/all/jquery-1.8.0.min.js
- http://code.jque.re/js/all/jquery-1.8.0.js
- http://code.jque.re/js/all/jquery-1.7.2.min.js
- http://code.jque.re/js/all/jquery-1.7.2.js
- http://code.jque.re/js/all/jquery-1.7.1.min.js
- http://code.jque.re/js/all/jquery-1.7.1.js
- http://code.jque.re/js/all/jquery-1.7.min.js
- http://code.jque.re/js/all/jquery-1.7.js
- http://code.jque.re/js/all/jquery-1.6.4.min.js
- http://code.jque.re/js/all/jquery-1.6.4.js
- http://code.jque.re/js/all/jquery-1.6.3.min.js
- http://code.jque.re/js/all/jquery-1.6.3.js
- http://code.jque.re/js/all/jquery-1.6.2.min.js
- http://code.jque.re/js/all/jquery-1.6.2.js
- http://code.jque.re/js/all/jquery-1.6.1.min.js
- http://code.jque.re/js/all/jquery-1.6.1.js
- http://code.jque.re/js/all/jquery-1.6rc1.min.js
- http://code.jque.re/js/all/jquery-1.6rc1.js
- http://code.jque.re/js/all/jquery-1.6b1.min.js
- http://code.jque.re/js/all/jquery-1.6b1.js
- http://code.jque.re/js/all/jquery-1.6.min.js
- http://code.jque.re/js/all/jquery-1.6.js
- http://code.jque.re/js/all/jquery-1.5rc1.min.js
- http://code.jque.re/js/all/jquery-1.5rc1.js
- http://code.jque.re/js/all/jquery-1.5b1.min.js
- http://code.jque.re/js/all/jquery-1.5b1.js
- http://code.jque.re/js/all/jquery-1.5.min.js
- http://code.jque.re/js/all/jquery-1.5.js
- http://code.jque.re/js/all/jquery-1.5.2rc1.min.js
- http://code.jque.re/js/all/jquery-1.5.2rc1.js
- http://code.jque.re/js/all/jquery-1.5.2.min.js
- http://code.jque.re/js/all/jquery-1.5.2.js
- http://code.jque.re/js/all/jquery-1.5.1rc1.min.js
- http://code.jque.re/js/all/jquery-1.5.1rc1.js
- http://code.jque.re/js/all/jquery-1.5.1.min.js
- http://code.jque.re/js/all/jquery-1.5.1.js
- http://code.jque.re/js/all/jquery-1.4rc1.min.js
- http://code.jque.re/js/all/jquery-1.4rc1.js
- http://code.jque.re/js/all/jquery-1.4a2.min.js
- http://code.jque.re/js/all/jquery-1.4a2.js
- http://code.jque.re/js/all/jquery-1.4a1.min.js
- http://code.jque.re/js/all/jquery-1.4a1.js
- http://code.jque.re/js/all/jquery-1.4.min.js
- http://code.jque.re/js/all/jquery-1.4.js
- http://code.jque.re/js/all/jquery-1.4.4rc3.min.js
- http://code.jque.re/js/all/jquery-1.4.4rc3.js
- http://code.jque.re/js/all/jquery-1.4.4rc2.min.js
- http://code.jque.re/js/all/jquery-1.4.4rc2.js
- http://code.jque.re/js/all/jquery-1.4.4rc1.min.js
- http://code.jque.re/js/all/jquery-1.4.4rc1.js
- http://code.jque.re/js/all/jquery-1.4.4.min.js
- http://code.jque.re/js/all/jquery-1.4.4.js
- http://code.jque.re/js/all/jquery-1.4.3rc2.min.js
- http://code.jque.re/js/all/jquery-1.4.3rc2.js
- http://code.jque.re/js/all/jquery-1.4.3rc1.min.js
- http://code.jque.re/js/all/jquery-1.4.3rc1.js
- http://code.jque.re/js/all/jquery-1.4.3.min.js
- http://code.jque.re/js/all/jquery-1.4.3.js
- http://code.jque.re/js/all/jquery-1.4.2.min.js
- http://code.jque.re/js/all/jquery-1.4.2.js
- http://code.jque.re/js/all/jquery-1.4.1.min.js
- http://code.jque.re/js/all/jquery-1.4.1.js
- http://code.jque.re/js/all/jquery-1.3rc2.pack.js
- http://code.jque.re/js/all/jquery-1.3rc2.min.js
- http://code.jque.re/js/all/jquery-1.3rc2.js
- http://code.jque.re/js/all/jquery-1.3rc1.pack.js
- http://code.jque.re/js/all/jquery-1.3rc1.min.js
- http://code.jque.re/js/all/jquery-1.3rc1.js
- http://code.jque.re/js/all/jquery-1.3b2.pack.js
- http://code.jque.re/js/all/jquery-1.3b2.min.js
- http://code.jque.re/js/all/jquery-1.3b2.js
- http://code.jque.re/js/all/jquery-1.3b1.pack.js
- http://code.jque.re/js/all/jquery-1.3b1.min.js
- http://code.jque.re/js/all/jquery-1.3b1.js
- http://code.jque.re/js/all/jquery-1.3.pack.js
- http://code.jque.re/js/all/jquery-1.3.min.js
- http://code.jque.re/js/all/jquery-1.3.js
- http://code.jque.re/js/all/jquery-1.3.2.pack.js
- http://code.jque.re/js/all/jquery-1.3.2.min.js
- http://code.jque.re/js/all/jquery-1.3.2.js
- http://code.jque.re/js/all/jquery-1.3.1rc1.pack.js
- http://code.jque.re/js/all/jquery-1.3.1rc1.min.js
- http://code.jque.re/js/all/jquery-1.3.1rc1.js
- http://code.jque.re/js/all/jquery-1.3.1.pack.js
- http://code.jque.re/js/all/jquery-1.3.1.min.js
- http://code.jque.re/js/all/jquery-1.3.1.js
- http://code.jque.re/js/all/jquery-1.2.pack.js
- http://code.jque.re/js/all/jquery-1.2.min.js
- http://code.jque.re/js/all/jquery-1.2.js
- http://code.jque.re/js/all/jquery-1.2.6.pack.js
- http://code.jque.re/js/all/jquery-1.2.6.min.js
- http://code.jque.re/js/all/jquery-1.2.6.js
- http://code.jque.re/js/all/jquery-1.2.5.pack.js
- http://code.jque.re/js/all/jquery-1.2.5.min.js
- http://code.jque.re/js/all/jquery-1.2.5.js
- http://code.jque.re/js/all/jquery-1.2.4b.pack.js
- http://code.jque.re/js/all/jquery-1.2.4b.min.js
- http://code.jque.re/js/all/jquery-1.2.4b.js
- http://code.jque.re/js/all/jquery-1.2.4a.pack.js
- http://code.jque.re/js/all/jquery-1.2.4a.min.js
- http://code.jque.re/js/all/jquery-1.2.4a.js
- http://code.jque.re/js/all/jquery-1.2.4.pack.js
- http://code.jque.re/js/all/jquery-1.2.4.min.js
- http://code.jque.re/js/all/jquery-1.2.4.js
- http://code.jque.re/js/all/jquery-1.2.3b.pack.js
- http://code.jque.re/js/all/jquery-1.2.3b.min.js
- http://code.jque.re/js/all/jquery-1.2.3b.js
- http://code.jque.re/js/all/jquery-1.2.3a.pack.js
- http://code.jque.re/js/all/jquery-1.2.3a.min.js
- http://code.jque.re/js/all/jquery-1.2.3a.js
- http://code.jque.re/js/all/jquery-1.2.3.pack.js
- http://code.jque.re/js/all/jquery-1.2.3.min.js
- http://code.jque.re/js/all/jquery-1.2.3.js
- http://code.jque.re/js/all/jquery-1.2.2b2.pack.js
- http://code.jque.re/js/all/jquery-1.2.2b2.min.js
- http://code.jque.re/js/all/jquery-1.2.2b2.js
- http://code.jque.re/js/all/jquery-1.2.2b.pack.js
- http://code.jque.re/js/all/jquery-1.2.2b.min.js
- http://code.jque.re/js/all/jquery-1.2.2b.js
- http://code.jque.re/js/all/jquery-1.2.2.pack.js
- http://code.jque.re/js/all/jquery-1.2.2.min.js
- http://code.jque.re/js/all/jquery-1.2.2.js
- http://code.jque.re/js/all/jquery-1.2.1.pack.js
- http://code.jque.re/js/all/jquery-1.2.1.min.js
- http://code.jque.re/js/all/jquery-1.2.1.js
- http://code.jque.re/js/all/jquery-1.1b.pack.js
- http://code.jque.re/js/all/jquery-1.1b.js
- http://code.jque.re/js/all/jquery-1.1a.pack.js
- http://code.jque.re/js/all/jquery-1.1a.js
- http://code.jque.re/js/all/jquery-1.1.pack.js
- http://code.jque.re/js/all/jquery-1.1.js
- http://code.jque.re/js/all/jquery-1.1.4.pack.js
- http://code.jque.re/js/all/jquery-1.1.4.js
- http://code.jque.re/js/all/jquery-1.1.3a.js
- http://code.jque.re/js/all/jquery-1.1.3.pack.js
- http://code.jque.re/js/all/jquery-1.1.3.js
- http://code.jque.re/js/all/jquery-1.1.3.1.pack.js
- http://code.jque.re/js/all/jquery-1.1.3.1.min.js
- http://code.jque.re/js/all/jquery-1.1.3.1.js
- http://code.jque.re/js/all/jquery-1.1.2.pack.js
- http://code.jque.re/js/all/jquery-1.1.2.js
- http://code.jque.re/js/all/jquery-1.1.1.pack.js
- http://code.jque.re/js/all/jquery-1.1.1.js
- http://code.jque.re/js/all/jquery-1.0.pack.js
- http://code.jque.re/js/all/jquery-1.0.js
- http://code.jque.re/js/all/jquery-1.0.4.pack.js
- http://code.jque.re/js/all/jquery-1.0.4.js
- http://code.jque.re/js/all/jquery-1.0.3.pack.js
- http://code.jque.re/js/all/jquery-1.0.3.js
- http://code.jque.re/js/all/jquery-1.0.2.pack.js
- http://code.jque.re/js/all/jquery-1.0.2.js
- http://code.jque.re/js/all/jquery-1.0.1.pack.js
- http://code.jque.re/js/all/jquery-1.0.1.js
